package az.ingress.TeacherAPITask2.repository;

import az.ingress.TeacherAPITask2.model.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TeacherRepository extends JpaRepository<Teacher, Integer> {
}
