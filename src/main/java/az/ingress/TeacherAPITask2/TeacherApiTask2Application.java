package az.ingress.TeacherAPITask2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeacherApiTask2Application {

	public static void main(String[] args) {
		SpringApplication.run(TeacherApiTask2Application.class, args);
	}

}
