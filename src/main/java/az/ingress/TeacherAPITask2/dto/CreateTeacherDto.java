package az.ingress.TeacherAPITask2.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;

@Data
public class CreateTeacherDto {

    private String name;
    private String faculty;
    @JsonFormat
    private LocalDate birthdate;
    String password;
}
