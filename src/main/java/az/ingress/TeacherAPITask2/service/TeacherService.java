package az.ingress.TeacherAPITask2.service;


import az.ingress.TeacherAPITask2.dto.CreateTeacherDto;
import az.ingress.TeacherAPITask2.dto.TeacherDto;
import az.ingress.TeacherAPITask2.dto.UpdateTeacherDto;
import az.ingress.TeacherAPITask2.model.Teacher;
import az.ingress.TeacherAPITask2.repository.TeacherRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TeacherService {

    private final TeacherRepository teacherRepository;
    private final ModelMapper modelMapper;

    public TeacherService(TeacherRepository teacherRepository, ModelMapper modelMapper) {
        this.teacherRepository = teacherRepository;
        this.modelMapper = modelMapper;
    }

    public void create(CreateTeacherDto dto) {
        //Teacher teacher = new Teacher();
        //teacher.setName(dto.getName());
        //teacher.setFaculty(dto.getFaculty());
        //teacher.setBirthdate(dto.getBirthdate());
        //teacher.setPassword(dto.getPassword());
        Teacher teacher = modelMapper.map(dto,Teacher.class);
        teacherRepository.save(teacher);
    }

    public void update(UpdateTeacherDto dto) {
        Optional<Teacher> entity = teacherRepository.findById(dto.getId());
        entity.ifPresent(teacher1 -> {
            teacher1.setName(dto.getName());
            teacher1.setFaculty(dto.getFaculty());
            teacher1.setBirthdate(dto.getBirthdate());
            teacherRepository.save(teacher1);
        });
    }

    public TeacherDto get(Integer id) {
        Teacher teacher = teacherRepository.findById(id).get();
        //TeacherDto teacherDto = new TeacherDto();
        //teacherDto.setId(teacher.getId());
        //teacherDto.setName(teacher.getName());
        //teacherDto.setFaculty(teacher.getFaculty());
        //teacherDto.setBirthdate(teacher.getBirthdate());
        TeacherDto teacherDto = modelMapper.map(teacher, TeacherDto.class);
        return teacherDto;
    }

    public void delete(Integer id) {
        teacherRepository.deleteById(id);
    }

    public List<Teacher> getAll() {
        return teacherRepository.findAll();
    }
}
