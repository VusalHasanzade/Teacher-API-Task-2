package az.ingress.TeacherAPITask2.controller;

import az.ingress.TeacherAPITask2.dto.CreateTeacherDto;
import az.ingress.TeacherAPITask2.dto.TeacherDto;
import az.ingress.TeacherAPITask2.dto.UpdateTeacherDto;
import az.ingress.TeacherAPITask2.model.Teacher;
import az.ingress.TeacherAPITask2.service.TeacherService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("teacher")
public class TeacherController {

    private final TeacherService teacherService;

    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @PostMapping
    public void createTeacher(@RequestBody CreateTeacherDto teacher) {
    teacherService.create(teacher);
    }

    @PutMapping
    public void updateTeacher(@RequestBody UpdateTeacherDto teacher) {
        teacherService.update(teacher);
    }

    @GetMapping("/{id}")
    public TeacherDto getTeacher(@PathVariable Integer id) {
    return teacherService.get(id);
    }

    @DeleteMapping("/{id}")
    public void deleteTeacher(@PathVariable Integer id) {
        teacherService.delete(id);
    }

    @GetMapping
    public List<Teacher> getAll(){
        return teacherService.getAll();
    }

}
